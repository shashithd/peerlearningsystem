var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var ejs = require('ejs');
var mongoDBConnection = require('./routes/dbConnection');
var apiRouter = require('./routes/router');

var app = express();
var routes = require('./routes/router');

app.set('views', path.join(__dirname, 'view'));
app.engine('html', ejs.renderFile);
app.set('view engine', 'html');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bower_components', express.static(__dirname + '/bower_components'));
app.use('/uploads', express.static(__dirname + '/uploads'));

global.mongoDb = mongoDBConnection.getConnection();

app.use('/', routes);
app.use('/api/', apiRouter);

module.exports = app;
