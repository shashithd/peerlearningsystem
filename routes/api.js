var HR = require('../model/HR');
var multer  =   require('multer');
var upload = multer({ dest: './uploads/' });

var Api = function() {};




Api.getEmptyHelpRequest = function(req, res) {
  res.send(HR.basicHelpRequest);
};

Api.saveHelpRequest  = function(req, res) {
  var HRData = req.body.data;
  HRData.dateCreated = (new Date).getTime();
  HR.saveHelpRequest(HRData, function(err){
    if(err)
      res.status(500).send('Error Occured');

    res.send("Successfull Inserted");
  });
};

Api.getHelpRequests = function(req,res) {
  var data = req.body.data;
  HR.getHelpRequests(data, function(err, data){
    if(err)
      res.status(500).send('Error Occured');

    res.send(data);
  });
};

Api.updateHelpRequest  = function(req, res) {
  var HRData = req.body.data;
  HRData.dateModified = (new Date).getTime();
  HR.saveHelpRequest(HRData, function(err){
    if(err)
      res.status(500).send('Error Occured');

    res.send("Successfull Updated");
  });
};


Api.deleteHelpRequest  = function(req, res) {
  var HRData = req.body.data;
  HRData.dateModified = (new Date).getTime();
  HR.deleteHelpRequest(HRData, function(err){
    if(err)
      res.status(500).send('Error Occured');

    res.send("Successfull Deleted");
  });
};

Api.uploadFile  = function(req, res) {
  res.send(req.file.path);
};

Api.getHelpRequest = function(req, res){
  var data = req.body;
  HR.getHelpRequest(data, function(err, data){
    if(err)
      res.status(500).send('Error Occured');

    res.send(data);
  });
};

module.exports = Api;
