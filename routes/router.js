var express = require('express');
var router = express.Router();
var api = require('./api');
var multer  =   require('multer');
var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads/');
  },
  filename: function (req, file, callback) {
    callback(null, file.originalname + '-' + Date.now());
  }
});
var upload = multer({ storage : storage}).single('file');

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/year', function(req, res, next) {
  res.render('year', { title: 'Express' });
});
router.get('/pie_chart', function(req, res, next) {
  res.render('pie_chart', { title: 'Express' });
});
router.get('/chart', function(req, res, next) {
  res.render('chart', { title: 'Express' });
});
router.get('/tree', function(req, res, next) {
  res.render('tree', { title: 'Express' });
});
router.get('/trend', function(req, res, next) {
  res.render('trend', { title: 'Express' });
});


router.route('/createHelpRequest').get(api.getEmptyHelpRequest);
router.route('/getHelpRequests').post(api.getHelpRequests);
router.route('/saveHelpRequest').post(api.saveHelpRequest);
router.route('/updateHelpRequest').post(api.updateHelpRequest);
router.route('/deleteHelpRequest').post(api.deleteHelpRequest);
router.route('/upload').post(upload, api.uploadFile);
router.route('/getHelpRequest').post(api.getHelpRequest);

module.exports = router;
