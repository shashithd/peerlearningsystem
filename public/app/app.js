var app = angular.module('pls', ['ui.router', 'ui.tinymce','ngSanitize', 'ngAnimate', 'ui.bootstrap']);
app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
    .state('home', {
        url : '/',
        templateUrl : 'app/plshome/home.html',
        controller : 'homeCtrl'
        })
    .state('createHelpRequest', {
          url : '/createHelpRequest:topic',
          templateUrl : 'app/createHelpRequest/createHelpRequest.html',
          controller : 'createHelpRequestCtrl'

        })
    .state('viewHelpRequest', {
          url : '/viewHelpRequest/:id',
          templateUrl : 'app/viewHelpRequest/viewHelpRequest.html',
          controller : 'viewHelpRequestCtrl'
        })
    .state('instructor', {
          url : '/instructor',
          templateUrl : 'app/instructor/instructor.html',
          controller : 'instructorCtrl'
        })
        ;

});
app.run(function ($http, $rootScope){
  $rootScope.userName = "shashith";
  $rootScope.userId = "123";
  $rootScope.courseId = "1";
  $rootScope.courseName = "Computer Science";
});
