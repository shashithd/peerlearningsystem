app.directive('helprequest', function($rootScope, $http) {
    return {
        restrict: 'AE',
        transclude: false,
        scope: {
            id: "=",

        },
        templateUrl: 'app/viewHelpRequest/helpRequest/helpRequest.html',
        controller: function($scope, $rootScope) {
            $scope.showeditor="hide";
            $scope.showeditorInvert = "show";

            $scope.max = 10;
            $scope.isReadonly = false;

            $scope.hoveringOver = function(value) {
             $scope.overStar = value;
             $scope.percent = 100 * (value / $scope.max);
            };

        },
        link: function(scope, element, attrs) {
          scope.$watch('id', function(id){
            if(id !== undefined) {
              $http.post('/api/getHelpRequest',{"id": id})
              .success(function(data){
                console.log(data);
                scope.helpRequest = data.hr[0];
                scope.helpRequest.rating = data.hr[0].rating;
                scope.helpRequest.relatedHr = ["Which property of water that causes it to rise in a tube?","What is The process in which liquid water becomes a gas is called evaporation."];
              })
              .error(function(err){
                console.log(err);
              });
            }
          });
          scope.enableTinyMce = function() {
              scope.showeditor = "show";
              scope.showeditorInvert = "hide";
          };
          scope.disableTinyMce = function() {
              scope.showeditor = "hide";
              scope.showeditorInvert = "show";
          };

          scope.publishAnswer = function() {
              var tinyMce = document.getElementsByClassName("mceEditor");
              var answerHtml = tinyMCE.activeEditor.getContent({format : 'html'});
              var milliseconds = (new Date).getTime();
              var answer = {"answer": answerHtml, "userId":$rootScope.userId, "userName":$rootScope.userName, "id":milliseconds, "rating":1};
              scope.helpRequest.answers.push(answer);
              scope.updateQuestion();
              scope.showeditor = "hide";
              scope.showeditorInvert = "show";

          };

          scope.updateQuestion = function() {
            $http.post('/api/updateHelpRequest',{data: scope.helpRequest}).success(
                   function(data, status, headers, config) {
                     console.log(data);
                   }).error(function(err) {
                     console.log(err);
                   });
          };

          scope.rate = function() {
            scope.updateQuestion();
          };



          tinymce.init({
                    mode : "specific_textareas",
                    editor_selector : "mceEditor",
                    theme: 'modern',
                    plugins: [
                      'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                      'searchreplace wordcount visualblocks visualchars code fullscreen',
                      'insertdatetime media nonbreaking save table contextmenu directionality',
                      'emoticons template paste textcolor colorpicker textpattern imagetools'
                    ],
                    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    toolbar2: 'print preview media | forecolor backcolor emoticons',
                    image_advtab: true,
                    templates: [
                      { title: 'Test template 1', content: 'Test 1' },
                      { title: 'Test template 2', content: 'Test 2' }
                    ],
                    content_css: [
                      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                      '//www.tinymce.com/css/codepen.min.css'
                    ],
                    file_picker_callback: function(callback, value, meta) {
                      // Provide file and text for the link dialog
                      if (meta.filetype == 'file') {
                        $('#fileUpload').click();
                        $scope.type = "file";
                        //$scope.uploadFile(fd);

                        $rootScope.$on("fileUploadA", function(event, data){
                          callback(data.path, {text: 'My text'});
                        })


                      }

                      // Provide image and alt text for the image dialog
                      if (meta.filetype == 'image') {
                        $('#imageUpload').click();
                        $scope.type = "image";
                        $rootScope.$on("imageUploadA", function(event, data){
                          callback(data.path, {alt: 'My alt text'});
                        })

                        //$scope.uploadFile(fd);


                        //$scope.uploadFile(fd);

                      }

                      // Provide alternative source and posted for the media dialog
                      if (meta.filetype == 'media') {
                        $('#mediaUpload').click();
                        $scope.type = "media";
                        $rootScope.$on("mediaUploadA", function(event, data){
                          callback(data.path, {source2: 'alt.ogg', poster: 'image.jpg'});
                        })

                        //$scope.uploadFile(fd);

                      }
                    }
                });

                scope.uploadfile = function(files) {
                  var fd = new FormData();
                  fd.append("file", files[0]);
                  $scope.fd = fd;

                  if (  $scope.fd) {
                      $http.post('/api/upload', $scope.fd, {
                          headers: {
                              'Content-Type': undefined
                          },
                          transformRequest: angular.identity
                      }).success(function(data) {
                          console.log(data);
                          if($scope.type === "file") {
                            $rootScope.$broadcast("fileUploadA",{"path": data});
                          } else if($scope.type === "image") {
                            $rootScope.$broadcast("imageUploadA",{"path": data});
                          } else {
                            $rootScope.$broadcast("mediaUploadA",{"path": data});
                          }
                      }).error(function(err) {
                      });
                  } else {
                      next(undefined);
                  }
                }

        }
    };
});
