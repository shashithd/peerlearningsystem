app.controller('createHelpRequestCtrl',function($scope, $http, $stateParams, $rootScope, $document, $location){
  //setting options for tinymce

  tinymce.init({
            mode : "specific_textareas",
            editor_selector : "mceEditor",
            theme: 'modern',
            plugins: [
              'advlist autolink lists link image charmap print preview hr anchor pagebreak',
              'searchreplace wordcount visualblocks visualchars code fullscreen',
              'insertdatetime media nonbreaking save table contextmenu directionality',
              'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true,
            templates: [
              { title: 'Test template 1', content: 'Test 1' },
              { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
              '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
              '//www.tinymce.com/css/codepen.min.css'
            ],
            file_picker_callback: function(callback, value, meta) {
              // Provide file and text for the link dialog
              if (meta.filetype == 'file') {
                $('#fileUpload').click();
                $scope.type = "file";
                //$scope.uploadFile(fd);

                $rootScope.$on("fileUpload", function(event, data){
                  callback(data.path, {text: 'My text'});
                })


              }

              // Provide image and alt text for the image dialog
              if (meta.filetype == 'image') {
                $('#imageUpload').click();
                $scope.type = "image";
                $rootScope.$on("imageUpload", function(event, data){
                  callback(data.path, {alt: 'My alt text'});
                })

                //$scope.uploadFile(fd);


                //$scope.uploadFile(fd);

              }

              // Provide alternative source and posted for the media dialog
              if (meta.filetype == 'media') {
                $('#mediaUpload').click();
                $scope.type = "media";
                $rootScope.$on("mediaUpload", function(event, data){
                  callback(data.path, {source2: 'alt.ogg', poster: 'image.jpg'});
                })

                //$scope.uploadFile(fd);

              }
            }
        });

  $scope.uploadfile = function(files) {
    var fd = new FormData();
    fd.append("file", files[0]);
    $scope.fd = fd;

    if (  $scope.fd) {
        $http.post('/api/upload', $scope.fd, {
            headers: {
                'Content-Type': undefined
            },
            transformRequest: angular.identity
        }).success(function(data) {
            console.log(data);
            if($scope.type === "file") {
              $rootScope.$broadcast("fileUpload",{"path": data});
            } else if($scope.type === "image") {
              $rootScope.$broadcast("imageUpload",{"path": data});
            } else {
              $rootScope.$broadcast("mediaUpload",{"path": data});
            }
        }).error(function(err) {
        });
    } else {
        next(undefined);
    }
    /*
        console.log("test");
        if ($scope.fd) {
            $http.post('/api/upload', $scope.fd, {
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: angular.identity
            }).success(function(data) {
            }).error(function(err) {
            });
        } else {
            next(undefined);
        }


    */
  };

  $scope.getEmptyQuestion = function() {
    $http.get('/api/createHelpRequest')
    .success(function(data) {
        $scope.tinymceModel = data.question;
        $scope.question = data;
        $scope.question.topic = $stateParams.topic;
        $scope.question.userId = $rootScope.userId;
        $scope.question.userName = $rootScope.userName;
        $scope.question.courseId = $rootScope.courseId;
        $scope.question.courseName = $rootScope.courseName;
    }).error(function(err) {
        alert("err ocuered");
    });
  };

  $scope.cancle = function() {
    $scope.getEmptyQuestion();
  };

  $scope.publish = function() {

    var tinyMce = document.getElementsByClassName("mceEditor");
    $scope.question.question = tinyMCE.activeEditor.getContent({format : 'html'});
    $http.post('/api/saveHelpRequest',{data: $scope.question}).success(
           function(data, status, headers, config) {
             $location.path("/");
             console.log(data);
           }).error(function(err) {
             console.log(err);
           });
  };

  $scope.getEmptyQuestion();



});
