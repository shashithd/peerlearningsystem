"use strict";
app.directive('collapsiblecontainer', function($rootScope, $http){
	return {
	    restrict: 'AE',
	    transclude : false,
	    scope: {
	        topic : "=",
	    },
	    templateUrl: '/app/plshome/collapiblecontainer/collapsibleContainer.html',
			controller: function ($scope){
				$scope.arrowtype = "down";
				$scope.showBody = true;
				$scope.max = 10;
				$scope.isReadonly = false;

				$scope.hoveringOver = function(value) {
				 $scope.overStar = value;
				 $scope.percent = 100 * (value / $scope.max);
				};
			},
	    link : function(scope, element, attrs) {

					scope.$watch('topic', function(value) {
                if (value !== undefined) {
									$http.post('/api/getHelpRequests', { data : {"topic": value}})
									.success(function(data){

											console.log(data.hr);
											scope.hrList = data.hr;
									})
									.error(function(err){
										console.log(err);
									});
                }

            });

	      	scope.toggleExpand = function(elem) {
						if(scope.arrowtype === "down") {
							scope.showBody = false;
						} else {
							scope.showBody = true;
						}
						scope.arrowtype = scope.arrowtype === "down" ? "up" : "down";

					};
	    }
	};
});
