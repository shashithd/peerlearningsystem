var express = require('express');
var AlchemyAPI = require('alchemy-api');
var alchemy = new AlchemyAPI('2ffe87e323e0f328e3046c2322f2788467673466');
var assert = require('assert');
var mongoDbCon = require('../routes/dbConnection');
var mongodb = require("mongodb");
var request = require('request');
var xml2js = require('xml2js');
var parser = new xml2js.Parser();

var HR = function() {};
module.exports = HR;

HR.saveHelpRequest = function(data, callback) {
  var ObjectId = mongodb.ObjectID;
  var dbCon = mongoDbCon.getConnection();
  var id = data._id;
  delete data._id;

  var keywords = HR.getKeywords(data.question, function(keywords) {
    data.keyWords = keywords;
    var IEELinks = HR.getIEELinks(keywords, function(IEELinks){
      data.relatedArticles = IEELinks;

      if(id === null || id === undefined) {
        dbCon.collection('helpRequest').insertOne(data, function(err, result) {
          if(err)
            callback(err);

          callback();
        });
      } else {
        dbCon.collection('helpRequest').update({_id: ObjectId(id)}, { $set: data },  { upsert: true },function(err, result) {
          console.log(err);
          if(err)
            callback(err);

          callback();
        });
      }
    });

  });



};

HR.getHelpRequest = function(data, callback) {
  var id = data.id;
  var response = {};
  response.hr = [];
  var ObjectId = mongodb.ObjectID;
  var dbCon = mongoDbCon.getConnection();
  var cursor = dbCon.collection('helpRequest').find({
      _id: { $in: [ ObjectId(id) ] }
   });
  if (cursor) {
        cursor.each(function(err, doc) {
            if (doc != null) {
                response.hr.push(doc);
            } else {
                callback(err,response);
            }
        });
    } else {
        callback({
            "hr": []
        });
    }
};

HR.getHelpRequests = function(data, callback) {
  var topic = data.topic;
  var response = {};
  response.hr = [];
  var dbCon = mongoDbCon.getConnection();
  var cursor = dbCon.collection('helpRequest').find({topic: topic});

  if (cursor) {
        cursor.each(function(err, doc) {
            if (doc != null) {
                response.hr.push(doc);
            } else {
                callback(err,response);
            }
        });
    } else {
        callback({
            "hr": []
        });
    }
};

HR.deleteHelpRequest = function(data, callback) {
  var ObjectId = mongodb.ObjectID;
  var dbCon = mongoDbCon.getConnection();
  var id = data.id;
  if(id !== null || id !== undefined) {
    dbCon.collection('helpRequest').remove( {_id: ObjectId(id)}, function(err, result) {
      console.log(err);
      if(err)
        callback(err);

      callback();
    });
  } else {
    callback("No item deleted. Invalid Id");
  }

};

HR.basicHelpRequest = {
    "title":"",
    "topic":"",
    "userName":"",
    "userId":"",
    "courseName":"",
    "courseId":"",
    "state":"",
    "question":"",
    "answers":[],
    "votes":0,
    "rating":1,
    "likes":0,
    "keyWords":[],
    "relatedHr":[],
    "relatedArticles":[],
    "dateCreated":"",
    "dateModified":""
  };

HR.getKeywords = function(data, callback) {
	var topKeywords = [];
	alchemy.keywords(data, {}, function(err, response) {


			var keywords = response.keywords;

			for (var keyword in keywords) {
				if (keywords.hasOwnProperty(keyword)) {
				  if(keywords[keyword].relevance > 0.5){
					topKeywords.push(keywords[keyword].text);
					}
				}
			}
      callback(topKeywords);

	});
};

HR.getIEELinks = function(keyWords, callback) {
	var iee = [];
	var ieeResult = {};
	var rec ={};
	var parser = new xml2js.Parser();

	var wordList = "";

		for(var keyWord in keyWords){
			wordList = wordList + keyWords[keyWord] + ',';
		}
		wordList = wordList.slice(0,-1);
    wordList = "bio molecule";
    console.log(wordList);

	request('http://ieeexplore.ieee.org/gateway/ipsSearch.jsp?querytext='+wordList+'&au=Wang&hc=10&rs=1&sortfield=ti&sortorder=asc', function (error, response, body) {
				if (error) {
          callback(ieeResult);
				}
				var xmldoc = require('xmldoc');
				parser.parseString(body, function (err, result) {

        if(result.root !== undefined) {
          for (var record in result.root.document) {

  					var rec = {
  						"title" : result.root.document[record].title,//(record.root.document)[0].title,
  						"author" : result.root.document[record].authors,//(record.root.document)[0].authors,
  						"url" : result.root.document[record].mdurl//(record.root.document)[0].mdurl
  					};
  				  iee.push(rec);

  			   }
           ieeResult = iee;
        }


      callback(ieeResult);
			});
	});
};
